package com.boyma.testdown;

import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import okhttp3.ResponseBody;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final FileDownloadService downloadService = ServiceGenerator.createService(FileDownloadService.class);

        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadService.downloadFileWithDynamicUrlSync("https://r1---sn-5cpnnoxu-up5l.googlevideo.com/videoplayback?lmt=1540922732762022&key=yt6&dur=1985.747&mt=1541326772&itag=22&pl=24&ei=JMjeW7eSOJvigAfZkLGYCw&ms=au%2Crdu&mm=31%2C29&expire=1541348485&ipbits=0&initcwndbps=492500&mime=video%2Fmp4&fvip=1&c=WEB&id=o-ALg2axlfiMbK-hmdH5J4EN8BYorUK5G9hvZJvvxLrj8a&ratebypass=yes&ip=77.242.26.57&mn=sn-5cpnnoxu-up5l%2Csn-4g5edns6&requiressl=yes&source=youtube&signature=1B294EF441D298560BEF8E480557147546E2DF4C.4522F3110163393FFE68EFA11A341FA029E55126&sparams=dur%2Cei%2Cid%2Cinitcwndbps%2Cip%2Cipbits%2Citag%2Clmt%2Cmime%2Cmm%2Cmn%2Cms%2Cmv%2Cpl%2Cratebypass%2Crequiressl%2Csource%2Cexpire&mv=m&txp=4531432&video_id=cshPYRLeqho&title=Mitsubishi+Outlander+XL+%D0%B7%D0%B0+650+%D1%82%D1%8B%D1%81%D1%8F%D1%87+%D1%80%D1%83%D0%B1%D0%BB%D0%B5%D0%B9.+Anton+Avtoman.")
                        .subscribeOn(Schedulers.io())
                        .observeOn(Schedulers.io())
                        .unsubscribeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Action1<ResponseBody>() {
                                       @Override
                                       public void call(ResponseBody responseBody) {
                                           File futureStudioIconFile = new File(getApplicationContext().getCacheDir(), "updata");
                                           InputStream inputStream = null;
                                           FileOutputStream fos = null;
                                           BufferedInputStream bis = null;
                                           try {
                                               inputStream = responseBody.byteStream();
                                               fos = new FileOutputStream(futureStudioIconFile);
                                               bis = new BufferedInputStream(inputStream);
                                               byte[] fileReader = new byte[1024];
                                               long fileSize = responseBody.contentLength();
                                               System.out.println("file size: " + fileSize);

                                               int len;
                                               int total = 0;
                                               while ((len = bis.read(fileReader)) != -1) {
                                                   fos.write(fileReader, 0, len);
                                                   total += len;
                                                   //获取当前下载量,更新progressdialog
                                                   //更新notification
                                                   System.out.println("file download: " + total + " of " + fileSize);
                                               }
                                               fos.close();
                                               bis.close();
                                               inputStream.close();
                                           } catch (IOException e) {
                                               e.printStackTrace();
                                           }
                                       }
                                   }, new Action1<Throwable>() {
                                       @Override
                                       public void call(Throwable throwable) {
                                           throwable.printStackTrace();
                                       }
                                   }

                        );


            }
        });


    }
}
